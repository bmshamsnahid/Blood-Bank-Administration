import { Component, OnInit } from '@angular/core';
import {User} from '../model/users';
import {UserService} from '../services/user';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  name: string;
  email: string;
  password: string;
  user: User;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.user = new User();
  }

  onClickSignUp() {
    if (!this.email || !this.password || !this.name) {
      console.log('Invalid email or password.');
    } else {
      this.user.name = this.name;
      this.user.email = this.email;
      this.user.password = this.password;
      // this.user.isAdmin = false;
      this.userService.createUser(this.user)
        .subscribe((response) => {
          if (response.success) {
            console.log('User created.');
          }
        });
    }
  }

}
