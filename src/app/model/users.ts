export class User {
  constructor() {}
  _id: string;
  name: string;
  number: string;
  email: string;
  password: string;
  createdDate: string;
  role: string;
  isAdmin: string;
  location: string;
  bloodGroup: string;
  isApproved: boolean;
}
