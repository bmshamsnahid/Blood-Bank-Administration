import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SignInComponent} from './sign-in/sign-in.component';
import {SignUpComponent} from './sign-up/sign-up.component';
import {SignInService} from './services/sign-in';
import {UserService} from './services/user';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import {AuthGuardService} from './services/auth-guard.service';
import { HomeComponent } from './home/home.component';
import { LogoutComponent } from './logout/logout.component';
import {AuthService} from './services/auth.service';
import {NavigationComponent} from './navigation/navigation.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LogoutComponent,
    SignInComponent,
    SignUpComponent,
    NavigationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot([
      { path: 'home', canActivate: [ AuthGuardService ],  component: HomeComponent },
      { path: 'sign-in', component: SignInComponent },
      { path: 'sign-up', component: SignUpComponent },
      { path: 'logout', canActivate: [ AuthGuardService ], component: LogoutComponent },
      { path: '', redirectTo: 'sign-in', pathMatch: 'full' },
      { path: '**', redirectTo: 'sign-in', pathMatch: 'full' }
    ])
  ],
  providers: [SignInService, UserService, AuthGuardService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
