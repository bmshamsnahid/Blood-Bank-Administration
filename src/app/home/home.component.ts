import { Component, OnInit } from '@angular/core';
import {UserService} from '../services/user';
import {User} from '../model/users';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  users: User[] = [];

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getAllUsers()
      .subscribe((response) => {
        if (response.success) {
          this.users = response.data;
          console.log(this.users);
        }
      });
  }

  updateUsers(user: User) {
    user.isApproved = !user.isApproved;
    this.userService.updateUser(user)
      .subscribe((response) => {
        console.log(response);
      });
  }

}
