import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../services/auth.service';
import {User} from '../model/users';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  email: string;
  password: string;
  user: User;

  constructor(private authService: AuthService,
              private router: Router) {
    this.user = new User();
  }

  ngOnInit() {
  }

  onClickSignIn() {
    if (!this.email || !this.password) {
      console.log('Invalid email or password.');
    } else {
      console.log('Send ing requeies');
      this.user.email = this.email;
      this.user.password = this.password;
      this.authService.userLogin(this.user)
        .subscribe((response) => {
          this.router.navigate(['/home']);
        });
    }
  }

}
