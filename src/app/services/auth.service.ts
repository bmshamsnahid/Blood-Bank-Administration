import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {environment} from '../../environments/environment';
import {Router} from '@angular/router';
import {User} from '../model/users';

@Injectable()
export class AuthService {

  private currentUser: User;
  headers = new Headers();
  options = new RequestOptions();

  constructor(private http: Http,
              private router: Router) {
    this.headers.append('Content-Type', 'application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }

  isLoggedIn(): boolean {
    try {
      const currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
      const currentUser = currentUserObj.currentUser;

      if (currentUser) {
        this.currentUser = currentUser;
        if (this.currentUser.role === 'admin') {
          return true;
        }
      }
    } catch (e) {
      return false;
    }
    return false;
  }

  userLogin(user: any) {

    return this.http.post(`${environment.baseUrl}/api/auth/login`, JSON.stringify(user), this.options)
      .map((response: Response) => {
        if (response.json().success) {
          const userObj: any = {};
          userObj.currentUser = response.json().data;
          userObj.token = response.json().token;
          localStorage.setItem('currentUserObj', JSON.stringify(userObj));
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Error in authenticating the user');
        }
      });
  }

  logout(): void {
    this.currentUser = null;
    localStorage.removeItem('currentUserObj');
    this.router.navigate(['/sign-in']);
  }

}
