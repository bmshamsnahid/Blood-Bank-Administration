import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {User} from '../model/users';
import {environment} from '../../environments/environment';


@Injectable()
export class SignInService {

  headers = new Headers();
  options = new RequestOptions();
  currentUserObj: any;
  currentUser: User;
  token: string;

  constructor(private http: Http) {
    this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    if (this.currentUserObj) {
      this.currentUser = this.currentUserObj.currentUser;
      this.token = this.currentUserObj.token;
      this.headers.append('Authorization', this.token);
    }
    this.headers.append('Content-Type', 'application/json');
    this.options = new RequestOptions({ headers: this.headers });
  }

  isLoggedIn(): boolean {
    try {
      const currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
      const currentUser = currentUserObj.currentUser;

      if (currentUser) {
        this.currentUser = currentUser;
        return true;
      }
    } catch (e) {
      return false;
    }
    return false;
  }

  userSignIn(user) {
    const options = new RequestOptions({ headers: this.headers});

    return this.http.post(`${environment.baseUrl}/api/auth/login`, JSON.stringify(user), options)
      .map((response: Response) => {
        if (response.json().success) {
          localStorage.setItem('email', user.email);
          localStorage.setItem('password', user.password);
          const userObj: any = {};
          userObj.currentUser = response.json().data;
          userObj.token = response.json().token;
          localStorage.setItem('currentUserObj', JSON.stringify(userObj));
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal Server Error');
        }
      });
  }

}
