import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {User} from '../model/users';
import {environment} from '../../environments/environment';

@Injectable()
export class UserService {

  headers: Headers;
  options: RequestOptions;

  currentUserObj;
  user: User;
  token;

  constructor(public http: Http) {
    this.headers = new Headers();

    this.headers.append('Content-Type', 'application/json');

    try {
      this.currentUserObj = JSON.parse(localStorage.getItem('currentUserObj'));
    } catch (e) {
      console.log('Please sign in to get all the resources.');
    }

    if (typeof this.currentUserObj !== 'undefined' && this.currentUserObj != null) {
      this.user = this.currentUserObj.currentUser;
      this.token = this.currentUserObj.token;
      this.headers.append('Authorization', this.token);
    }

    this.options = new RequestOptions({ headers: this.headers});
  }

  getAllUsers() {
    return this.http.get(`${environment.baseUrl}/api/user`, this.options)
      .map((response: Response) => {
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal server error.');
        }
      });
  }

  createUser(user: User) {
    return this.http.post(`${environment.baseUrl}/api/user`, JSON.stringify(user), this.options)
      .map((response: Response) => {
        console.log(response);
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal server error.');
        }
      });
  }

  updateUser(user: User) {
    return this.http.patch(`${environment.baseUrl}/api/user/${user._id}`, JSON.stringify(user), this.options)
      .map((response: Response) => {
        console.log(response);
        if (response.json().success) {
          return response.json();
        } else if (response.json().message) {
          console.log(response.json().message);
        } else {
          console.log('Fatal server error.');
        }
      });
  }

}
